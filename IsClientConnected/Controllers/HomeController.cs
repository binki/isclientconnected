﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IsClientConnected.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        private void log<T>(T _s)
        {
            var s = _s.ToString();
            Console.WriteLine(s);
            System.Diagnostics.EventLog myLog = null;
            try
            {
                myLog = new System.Diagnostics.EventLog("MyNewLog") { Source = "MySource", };
            }
            catch
            {
                System.Diagnostics.EventLog.CreateEventSource("MySource", "MyNewLog");
                log(s);
                return;
            }
            myLog.WriteEntry("ASDF: "+s);
        }

        public async Task<ActionResult> Do()
        {
            log("o"+System.Threading.Thread.CurrentThread.ManagedThreadId);
            return await Task.Factory.StartNew<ActionResult>(
                () =>
                {
                    while (Response.IsClientConnected)
                    {
                        System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
                        log("oCONNECTED" + System.Threading.Thread.CurrentThread.ManagedThreadId);
                    }
                    log("oDISCONNECTED" + System.Threading.Thread.CurrentThread.ManagedThreadId);
                    return View();
                });
        }

        public async Task<ActionResult> DoViaToken()
        {
            log("n"+System.Threading.Thread.CurrentThread.ManagedThreadId);
            return await Task.Factory.StartNew<ActionResult>(
                () =>
                {
                    while (!Response.ClientDisconnectedToken.IsCancellationRequested)
                    {
                        System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
                        log("nCONNECTED" + System.Threading.Thread.CurrentThread.ManagedThreadId);
                    }
                    log("nDISCONNECTED" + System.Threading.Thread.CurrentThread.ManagedThreadId);
                    return View();
                });
        }
    }
}
